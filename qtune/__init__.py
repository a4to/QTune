import argparse
version = "0.1.0"


def getOpts():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model", type=str, required=True)
    parser.add_argument("-d", "--dataset", type=str, required=True)
    parser.add_argument("-o", "--output", type=str, required=True)
    return parser.parse_args()


model_path = getOpts().model
dataset_path = getOpts().dataset
output_dir = getOpts().output


if __name__ == "__main__":
    print("Model: {}".format(model_path))
    print("Dataset: {}".format(dataset_path))
    print("Output: {}".format(output_dir))
    print("Version: {}".format(version))

