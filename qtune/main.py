import torch
import sys
import os
import json
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig
from peft import prepare_model_for_kbit_training, LoraConfig, get_peft_model
from datasets import load_dataset, Dataset
from transformers import Trainer, TrainingArguments, DataCollatorForLanguageModeling
import qtune

opts = qtune.getOpts()
model_path = opts.model
dataset_path = opts.dataset
output_dir = opts.output

tokenizer, model, trainer, data, config, bnb_config = None, None, None, None, None, None

def print_trainable_parameters(model):
    trainable_params = 0
    all_param = 0
    for _, param in model.named_parameters():
        all_param += param.numel()
        if param.requires_grad:
            trainable_params += param.numel()
    print(
        f"trainable params: {trainable_params} || all params: {all_param} || trainable%: {100 * trainable_params / all_param}"
    )

def loadModel(model_path):
    global bnb_config, tokenizer, model
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_use_double_quant=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=torch.bfloat16
    )

    tokenizer = AutoTokenizer.from_pretrained(model_path)
    model = AutoModelForCausalLM.from_pretrained(model_path, quantization_config=bnb_config, device_map={"": 0})
    model.gradient_checkpointing_enable()
    model = prepare_model_for_kbit_training(model)

    # Apply LoraConfig
    global config
    config = LoraConfig(
        r=8,
        lora_alpha=32,
        target_modules=["query_key_value"],
        lora_dropout=0.05,
        bias="none",
        task_type="CAUSAL_LM"
    )
    model = get_peft_model(model, config)
    print_trainable_parameters(model)

def loadDataset(dataset_path):
    with open(dataset_path, 'r') as f:
        data_list = [json.loads(line) for line in f]

    # Convert to HuggingFace Dataset
    global data
    data = Dataset.from_dict({k: [dic[k] for dic in data_list] for k in data_list[0]})
    data = data.map(lambda samples: tokenizer(samples["text"]), batched=True)

def prepTrainer():
    tokenizer.pad_token = tokenizer.eos_token
    global trainer
    trainer = Trainer(
        model=model,
        train_dataset=data,
        args=TrainingArguments(
            per_device_train_batch_size=1,
            gradient_accumulation_steps=4,
            warmup_steps=2,
            max_steps=10,
            learning_rate=2e-4,
            fp16=True,
            logging_steps=1,
            output_dir=output_dir,
            optim="paged_adamw_8bit"
        ),
        data_collator=DataCollatorForLanguageModeling(tokenizer, mlm=False),
    )
    model.config.use_cache = False  # silence the warnings. Please re-enable for inference!

def train():
    trainer.train()

def save():
    model.save_pretrained(output_dir)

def main():
    loadModel(model_path)
    print_trainable_parameters(model)
    loadDataset(dataset_path)
    prepTrainer()
    train()
    save()

if __name__ == "__main__":
    main()
