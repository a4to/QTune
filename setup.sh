#!/usr/bin/env bash

which conda || {
  wget -O miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-py37_4.10.3-Linux-x86_64.sh
  chmod +x miniconda.sh
  bash ./miniconda.sh -b -f -p /usr/local
  rm miniconda.sh
  conda config --add channels conda-forge
  conda install -y mamba
  mamba update -qy --all
  mamba clean -qafy
}

eval "$(conda shell.bash hook)"

mamba env create -n CLM -f environment.yml

conda activate CLM

pip3 install \
  torch==2.0.1+cu118 torchvision==0.15.2+cu118 --extra-index-url https://download.pytorch.org/whl/cu118 \
  -r requirements.txt \
  -e .

mkdir out
echo -e "\n\nqtune -m "ehartford/WizardLM-13B-V1.0-Uncensored" -d ./qtune/datasets/quotes.jsonl -o ./out"

