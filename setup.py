from setuptools import setup, find_packages

setup(
    name='qtune',
    version='0.1.0',
    description='QLora Fine-tuning for NLP',
    author='Connor Etherington',
    author_email='connor@concise.cc',
    packages=find_packages(),
    install_requires=[
        'torch',
        'bitsandbytes',
        'datasets',
    ],
    dependency_links=[
        'git+https://github.com/huggingface/transformers.git',
        'git+https://github.com/huggingface/peft.git',
        'git+https://github.com/huggingface/accelerate.git',
    ],
    entry_points={
        'console_scripts': [
            'qtune=qtune.main:main'
        ]
    }
)
